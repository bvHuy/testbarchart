package com.example.testbarchart;

import android.content.Context;

import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.components.Description;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.interfaces.datasets.IBarDataSet;

import java.util.ArrayList;
import java.util.List;

public class MaxBarChart {
    private com.github.mikephil.charting.charts.BarChart barChart;
    private Description mDescription;
    private XAxis x1;
    private YAxis y1;
    private GetDataFromPreference getData;
    private List<Integer> list;
    private int length;
    private Context mContext;
    private String mChartName;

    public BarChart getBarChart() {
        return barChart;
    }

    public MaxBarChart(Description description, List<Integer> listMonth, Context context, String chartName) {
        this.mDescription = description;
        this.list = listMonth;
        this.mContext = context;
        mChartName = chartName;
        drawChart();
    }

    public void drawChart() {
        list = new ArrayList<>();
        getData = new GetDataFromPreference(mContext);
        barChart = new com.github.mikephil.charting.charts.BarChart(mContext);
        getData.doSave(list, mChartName);
        barChart.setDrawBarShadow(false);
        barChart.setDrawValueAboveBar(true);
        barChart.setDescription(mDescription);
        barChart.setMaxVisibleValueCount(100);
        barChart.setPinchZoom(false);
        barChart.setDrawGridBackground(true);
        x1 = barChart.getXAxis();
        x1.setGranularity(1f);
        x1.setCenterAxisLabels(true);
        y1 = barChart.getAxisLeft();
        y1.setDrawGridLines(true);
        y1.setSpaceTop(10f);
        barChart.getAxisRight().setEnabled(false);
        setDataToChart();
    }

    private void setDataToChart() {
        float groupSpace = 0.04f;
        float barSpace = 0.02f;
        float barWidth = 0.46f;
        List<Integer> testList = getData.loadGameSetting(mChartName);
        ArrayList<BarEntry> yVal1 = new ArrayList<BarEntry>();
        ArrayList<BarEntry> yVal2 = new ArrayList<BarEntry>();
        for (int i = 0; i < length; i++) {
            yVal1.add(new BarEntry(i, testList.get(i)));
            yVal2.add(new BarEntry(i, testList.get(length - i - 1)));
        }
        BarDataSet set1, set2;
        if (barChart.getData() != null && barChart.getData().getDataSetCount() > 0) {
            set1 = (BarDataSet) barChart.getData().getDataSetByIndex(0);
            set2 = (BarDataSet) barChart.getData().getDataSetByIndex(1);
            set1.setValues(yVal1);
            set2.setValues(yVal2);
            barChart.getData().notifyDataChanged();
            barChart.notifyDataSetChanged();
        } else {
            set1 = new BarDataSet(yVal1, "first");
            set1.setColor(mContext.getResources().getColor(R.color.shortchart));
            set2 = new BarDataSet(yVal2, "second");
            set2.setColor(mContext.getResources().getColor(R.color.highchart));
            ArrayList<IBarDataSet> dataSets = new ArrayList<IBarDataSet>();
            dataSets.add(set1);
            dataSets.add(set2);
            BarData data = new BarData((dataSets));
            barChart.setData(data);
        }
        barChart.getBarData().setBarWidth(barWidth);
        barChart.groupBars(0, groupSpace, barSpace);
        barChart.invalidate();
    }
}

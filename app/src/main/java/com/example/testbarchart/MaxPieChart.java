package com.example.testbarchart;

import android.content.Context;
import android.graphics.Color;

import com.github.mikephil.charting.components.Description;
import com.github.mikephil.charting.data.PieData;
import com.github.mikephil.charting.data.PieDataSet;
import com.github.mikephil.charting.data.PieEntry;
import com.github.mikephil.charting.formatter.PercentFormatter;
import com.github.mikephil.charting.utils.ColorTemplate;

import java.util.ArrayList;
import java.util.List;

public class MaxPieChart {
    private com.github.mikephil.charting.charts.PieChart pieChart;
    private String[] labels;
    private Context mContext;
    private String mChartName;
    private List<Integer> quantityList;

    public MaxPieChart(Context mContext, String mChartName, List<Integer> quantityList) {
        this.mContext = mContext;
        this.mChartName = mChartName;
        this.quantityList = quantityList;
    }

    private void drawChart() {
        pieChart = new com.github.mikephil.charting.charts.PieChart(mContext);
        pieChart.setUsePercentValues(true);

        ArrayList<PieEntry> yvalues = new ArrayList<PieEntry>();
        yvalues.add(new PieEntry(8f, "January", 0));
        yvalues.add(new PieEntry(15f, "February", 1));
        yvalues.add(new PieEntry(12f, "March", 2));
        yvalues.add(new PieEntry(25f, "April", 3));
        yvalues.add(new PieEntry(23f, "May", 4));
        yvalues.add(new PieEntry(17f, "June", 5));

        PieDataSet dataSet = new PieDataSet(yvalues, mChartName);
        PieData data = new PieData(dataSet);

        data.setValueFormatter(new PercentFormatter());
        pieChart.setData(data);
        Description description = new Description();
        description.setText(mChartName);
        pieChart.setDescription(description);
        pieChart.setDrawHoleEnabled(true);
        pieChart.setTransparentCircleRadius(58f);
        pieChart.setHoleRadius(58f);
        dataSet.setColors(ColorTemplate.VORDIPLOM_COLORS);
        data.setValueTextSize(13f);
        data.setValueTextColor(mContext.getResources().getColor(R.color.white));

    }
}

package com.example.testbarchart;


import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.github.mikephil.charting.charts.BarChart;

import java.util.ArrayList;

public class MyAdapter extends RecyclerView.Adapter<MyAdapter.ChartHolder> {
    Context context;
    LayoutInflater layoutInflater;
    MaxBarChart[] maxBarChart;

    public MyAdapter(Context context, MaxBarChart[] maxBarChart) {
        this.context = context;
        this.maxBarChart = maxBarChart;
    }

    @NonNull
    @Override
    public ChartHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        if (layoutInflater==null){
            layoutInflater = LayoutInflater.from(context);
        }

        return new ChartHolder(layoutInflater.inflate(R.layout.layout_item, viewGroup, false));
    }

    @Override
    public void onBindViewHolder(@NonNull ChartHolder chartHolder, int i) {
        if (chartHolder.chart!=null) {
        }
//        if (i%2==0){
        chartHolder.chart.setBackgroundColor(context.getResources().getColor(R.color.white));

//        }else {
//            chartHolder.ivSource.setBackgroundColor(Color.YELLOW);
//        }
    }

    @Override
    public int getItemCount() {
        return 3;
    }


    public class ChartHolder extends RecyclerView.ViewHolder{
        BarChart chart;
        public ChartHolder(@NonNull View itemView) {
            super(itemView);
            chart = itemView.findViewById(R.id.chart);
        }
    }
}